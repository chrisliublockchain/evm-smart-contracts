// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import { ERC20 } from '@openzeppelin/contracts/token/ERC20/ERC20.sol';
import { AccessControl } from '@openzeppelin/contracts/access/AccessControl.sol';
import { ERC20Burnable } from '@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol';

contract TestERC20Token is ERC20, ERC20Burnable, AccessControl {
  bytes32 public constant MINTER = keccak256("MINTER");

  uint256 constant initialSupply = 1000000 * (10 ** 18);

  constructor() ERC20("TestERC20Token", "PWT") {
    _mint(msg.sender, initialSupply);
    _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    _grantRole(MINTER, msg.sender);
  }

  function mint(address to, uint256 amount) public onlyRole(MINTER) {
    _mint(to, amount);
  }
}
