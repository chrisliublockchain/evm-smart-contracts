import { expect } from 'chai';
import { ethers } from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

import { TestERC20Token } from '../typechain-types';

describe('TestERC20Token (PWT)', () => {
  let decimals: number;
  let token: TestERC20Token;
  let signers: SignerWithAddress[];

  before(async () => {
    const contract = await ethers.getContractFactory('TestERC20Token');
    token = await contract.deploy();
    await token.deployed();
    signers = await ethers.getSigners();
  });

  it("Check if name = 'TestERC20Token'", async () => {
    const name = await token.name();
    expect(name).to.exist;
    expect(name).to.equal('TestERC20Token');
  });

  it("Check if symbol = 'PWT'", async () => {
    const symbol = await token.symbol();
    expect(symbol).to.exist;
    expect(symbol).to.equal('PWT');
  });

  it("Check if decimals = 18", async () => {
    decimals = await token.decimals();
    expect(decimals).to.exist;
    expect(decimals).to.equal(18);
  })

  it("Check if total supply = 1,000,000", async () => {
    const totalSupply = await token.totalSupply();
    expect(totalSupply).to.exist;
    expect(totalSupply.toString()).to.equal(ethers.utils.parseUnits('1000000', decimals));
  });

  it("Check if contract owner have full amount of total supply", async () => {
    const totalSupply = await token.totalSupply();
    const ownerBalance = await token.balanceOf(signers[0].address);
    expect(totalSupply.toString()).to.equal(ownerBalance);
  });

  it("Check if successfully transfer 1,000 token from owner to recipient", async () => {
    const amount = ethers.utils.parseUnits('1000', decimals).toString();
    await expect(token.transfer(signers[1].address, amount))
      .to.changeTokenBalances(
        token,
        [signers[0].address, signers[1].address],
        [`-${amount}`, amount],
      );
  });

  it("Check if total supply = 1,001,000 after minting 1,000 tokens", async () => {
    const originalTotalSupply = await token.totalSupply();
    const mintAmount = ethers.utils.parseUnits('1000', decimals);
    await token.mint(signers[0].address, ethers.utils.parseUnits('1000', decimals));
    const totalSupply = await token.totalSupply();
    expect(totalSupply.toString()).to.equal(mintAmount.add(originalTotalSupply));
  });

  it("Check if non-minter role will fail to execute mint function", async () => {
    await expect(token.connect(signers[1]).mint(signers[1].address, ethers.utils.parseUnits('1000', decimals)))
      .to.be.reverted;
  });
});
