import { expect } from 'chai';
import { ethers } from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

import { TestERC721Token } from '../typechain-types';

describe('TestERC721Token (PWNFT)', () => {
  let token: TestERC721Token;
  let signers: SignerWithAddress[];

  before(async () => {
    const contract = await ethers.getContractFactory('TestERC721Token');
    token = await contract.deploy();
    await token.deployed();
    signers = await ethers.getSigners();
  });

  it("Check if name = 'TestERC721Token'", async () => {
    const name = await token.name();
    expect(name).to.exist;
    expect(name).to.equal('TestERC721Token');
  });

  it("Check if symbol = 'PWNFT'", async () => {
    const symbol = await token.symbol();
    expect(symbol).to.exist;
    expect(symbol).to.equal('PWNFT');
  });

  it("Check if successfully mint a NFT using contract owner", async () => {
    await token.mint(signers[0].address, 'ipfs://bafyreifomndeaomklzciqfxylv3wqjp4q6yb2gsevxnv2bey4k6e3oolsu/metadata.json');
    const balance = await token.balanceOf(signers[0].address);
    const nftOwner = await token.ownerOf(1);
    expect(balance.toString()).to.equal('1');
    expect(nftOwner).to.equal(signers[0].address);
  });

  it("Check if successfully transfer NFT #1 to recipient", async () => {
    await token['safeTransferFrom(address,address,uint256)'](
      signers[0].address,
      signers[1].address,
      1,
    );
    const nftOwner = await token.ownerOf(1);
    expect(nftOwner).to.equal(signers[1].address);
  });
});
