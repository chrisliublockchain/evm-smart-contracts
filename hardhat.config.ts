import 'dotenv/config';
import '@nomicfoundation/hardhat-toolbox';
import { HardhatUserConfig } from 'hardhat/config';

import './tasks/deploy';
import './tasks/mintNFT';

const { ETHEREUM_GOERLI_NODE_URL, PRIVATE_KEY } = process.env;
const privateKey = String(PRIVATE_KEY);

const config: HardhatUserConfig = {
  solidity: "0.8.17",
  paths: {
    tests: './tests',
    sources: './contracts',
  },
  networks: {
    hardhat: {},
    goerli: {
      url: ETHEREUM_GOERLI_NODE_URL,
      accounts: [privateKey]
    }
  }
};

export default config;
