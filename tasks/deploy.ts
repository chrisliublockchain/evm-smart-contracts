import { task } from 'hardhat/config';

type Token = 'TestERC20Token' | 'TestERC721Token' | string;

task("deploy", "Deploy contract to blockchain")
  .addPositionalParam('token')
  .setAction(async (args, hre) => {
    const { ethers } = hre;
    const { token }: { token: Token } = args;

    const deploy = async (token: Token) => {
      const contractOwner = await ethers.getSigners();
      console.log(`Preparing contract deployment from '${contractOwner[0].address}'`);
    
      const contract = await ethers.getContractFactory(token);
      console.log(`Deploying ${token}...`);
    
      const tokenUnderDeployment = await contract.deploy();
      await tokenUnderDeployment.deployed();
      console.log(`${token} deployed at ${tokenUnderDeployment.address}`);
    }

    await deploy(token);
  });
