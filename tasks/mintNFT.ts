import { task } from 'hardhat/config';

type Token = 'TestERC721Token' | string;

task("mintNFT", "Mint NFT")
  .addPositionalParam('token')
  .addPositionalParam('metadataUrl')
  .addPositionalParam('contractAddress')
  .setAction(async (args, hre) => {
    const { ethers } = hre;
    const {
      token,
      metadataUrl,
      contractAddress,
    }: {
      token: Token,
      metadataUrl: string,
      contractAddress: string,
    } = args;

    const mintNFT = async (token: Token, metadataUrl: string, contractAddress: string) => {
      const contractOwner = await ethers.getSigners();
      console.log(`Preparing to mint a new NFT for '${contractOwner[0].address}'`);
    
      const contract = await ethers.getContractAt(token, contractAddress);
      await contract.mint(contractOwner[0].address, metadataUrl);
      console.log(`Minted a new NFT`);
    }

    await mintNFT(token, metadataUrl, contractAddress);
  });
