import 'dotenv/config';
import fs from 'fs';
import { NFTStorage, File } from 'nft.storage';

const uploadNFT = async () => {
  const token = String(process.env.NFT_STORAGE_API_KEY);
  const client = new NFTStorage({ token });
  const baseUrl = `${process.cwd()}/assets/output`;

  const nftCollection = (await fs.promises.readdir(`${process.cwd()}/assets/output`)).map((nft, index) => ({
    imageName: nft,
    imageUrl: `${baseUrl}/${nft}`,
    name: `Poro Wallet NFT #${index} - ${nft.split('.')[0].split('_')[1]}`,
    description: 'NFT randomly generated for Poro Wallet development purpose.',
  }));

  const metadata = [];
  for (const nft of nftCollection) {
    const { name, description, imageUrl, imageName } = nft;
    console.log(`Uploading '${name}'...`);
    const { url } = await client.store({
      name,
      description,
      image: new File(
        [await fs.promises.readFile(imageUrl)],
        imageName,
        { type: 'image/png' },
      ),
    });
    console.log(`Uploaded '${name}' at '${url}'`);
    metadata.push(url);
  }

  await fs.promises.writeFile(`${process.cwd()}/assets/metadata.json`, JSON.stringify(metadata, null, 4));
}

uploadNFT();
