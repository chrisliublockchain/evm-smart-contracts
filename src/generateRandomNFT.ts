import fs from 'fs';
import sharp from 'sharp';

const generateRandomNFT = async () => {
  const cryptos = `${process.cwd()}/assets/crypto`;
  const backgrounds = `${process.cwd()}/assets/background`;

  const cryptoLayers = (await fs.promises.readdir(cryptos)).map((image) => ({
    name: image.split('.')[0],
    path: `${cryptos}/${image}`,
  }));
  const backgroundLayers = (await fs.promises.readdir(backgrounds)).map((image) => ({
    name: image.split('.')[0],
    path: `${backgrounds}/${image}`,
  }));

  for (const background of backgroundLayers) {
    for (const crypto of cryptoLayers) {
      const filename = `${process.cwd()}/assets/output/${background.name}_${crypto.name}.png`;
      console.log(`Generating ${filename}`);
      await sharp(background.path)
        .composite([{ input: crypto.path }])
        .toFile(filename);
    }
  }
}

generateRandomNFT();
