# EVM Smart Contracts

Playground for developing random EVM smart contracts

## Local Environment Setup

### How to install everything

```bash
git clone git@gitlab.com:chrisliublockchain/evm-smart-contracts.git
cd evm-smart-contracts
npm ci
```

### How to use Hardhat

Write your contract under `/contracts` folder
Write the interaction script with contract under `/scripts` folder
Write contract test cases under `/tests` folder

```bash
// Compile contracts in /contracts folder
npx hardhat compile
// Start up a standalone local node for testing
npx hardhat node
npx hardhat run --network localhost scripts/deploy.ts
```

### How to deploy a token supported in this repository

```bash
npm run deploy:goerli TestERC20Token
```

### How to mint a new NFT to an address

```bash
npm run mintNFT:goerli TestERC721Token ipfs://bafyreifomndeaomklzciqfxylv3wqjp4q6yb2gsevxnv2bey4k6e3oolsu/metadata.json 0xcCECCbdc8d4E12932F5181D65aD1BD0FCEd3Cd24
```

### How to generate NFT

1. Prepare your layers of image to combine as a NFT collection in `asset` folder like `background` and `crypto`
2. Run `npx ts-node ./src/generateRandomNFT.ts` which generates all NFTs under `assets/output` directory
3. Add your nft.storage API key to the field `NFT_STORAGE_API_KEY` in `.env` file
4. Run `npx ts-node ./src/uploadNFT.ts` which generates a metadata file in `assets/metadata.json`
5. Use the list to mint NFT
